var c = document.getElementById("canvas");
var ctx = c.getContext("2d");

var presion = false;
var fin = false;
var puntuacion = 0;
var mapa = [];

//Array bidimensional para la creación del tablero (1 -> pared 0-> pasillo)
mapa[0] = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
mapa[1] = [1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1];
mapa[2] = [1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1];
mapa[3] = [1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1];
mapa[4] = [1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1];
mapa[5] = [1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1];
mapa[6] = [1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1];
mapa[7] = [1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1];
mapa[8] = [1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1];
mapa[9] = [1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1];
mapa[10] = [1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1];
mapa[11] = [1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1];
mapa[12] = [1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1];
mapa[13] = [1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1];
mapa[14] = [1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1];
mapa[15] = [1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1];
mapa[16] = [1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1];
mapa[17] = [1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1];
mapa[18] = [1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1];
mapa[19] = [1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1];
mapa[20] = [1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1];
mapa[21] = [1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1];
mapa[22] = [1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1];
mapa[23] = [1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1];
mapa[24] = [1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1];
mapa[25] = [1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1];
mapa[26] = [1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1];
mapa[27] = [1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1];
mapa[28] = [1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1];
mapa[29] = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];


var fantasma1 = {
    y: '',
    x: '',
    iniF: "false",
    direc: ""
};//mapa[1][1];
var fantasma2 = {
    y: '',
    x: '',
    iniF: "false",
    direc: ""
};//mapa[7][23];
var fantasma3 = {
    y: '',
    x: '',
    iniF: "false",
    direc: ""
};//mapa[24][2];
var jugador = {
    y: '',
    x: '',
    iniJ: "false",
    direc: ""
};//mapa[17][9]; 

function start() {
    //Creo un bucle para comprobar si ya se le ha asignado una posicion correcta a los fantasmas y al jugador.
    do {
        //En caso de no tener una posicion asignada hago un random para asignar un numero del 1 al 30 a las variables "x y" de los fantasmas y el jugador
        if (fantasma1.iniF == "false") {
            fantasma1.y = Math.floor(Math.random() * 30);//parseo el random a entero para que no haya decimales
            fantasma1.x = Math.floor(Math.random() * 30);

            if (mapa[fantasma1.y][fantasma1.x] == "  ") {
                mapa[fantasma1.y][fantasma1.x] = " F ";
                fantasma1.direc = direccion(fantasma1.y, fantasma1.x);
                fantasma1.iniF = "true";
            }
        }

        if (fantasma2.iniF == "false") {
            fantasma2.y = Math.floor(Math.random() * 30);
            fantasma2.x = Math.floor(Math.random() * 30);

            if (mapa[fantasma2.y][fantasma2.x] == "  ") {
                mapa[fantasma2.y][fantasma2.x] = " F ";
                fantasma2.direc = direccion(fantasma2.y, fantasma2.x);
                fantasma2.iniF = "true";
            }
        }

        if (fantasma3.iniF == "false") {
            fantasma3.y = Math.floor(Math.random() * 30);
            fantasma3.x = Math.floor(Math.random() * 30);

            if (mapa[fantasma3.y][fantasma3.x] == "  ") {
                mapa[fantasma3.y][fantasma3.x] = " F ";
                fantasma3.direc = direccion(fantasma3.y, fantasma3.x);
                fantasma3.iniF = "true";
            }
        }

        if (jugador.iniJ == "false") {
            jugador.y = Math.floor(Math.random() * 30);
            jugador.x = Math.floor(Math.random() * 30);

            if (mapa[jugador.y][jugador.x] == "  ") {
                mapa[jugador.y][jugador.x] = " J ";
                jugador.direc = direccion(jugador.y, jugador.x);
                jugador.iniJ = "true";
            }
        }

    } while (fantasma1.iniF == "false" || fantasma2.iniF == "false" || fantasma3.iniF == "false" || jugador.iniJ == "false");
    pintarMapaCanvas();
    //pintarMapa();
}

//Se recorre el array de mapa y se le asigna una F a los fantasmas con su color, una J al jugador y se pintan los pasillos
function pintarMapaCanvas(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    mapa.forEach((posicion,y) => {
        posicion.forEach((element,x) => {
            if (element == 1) {
                ctx.fillStyle = "purple"
                ctx.fillRect(x*20,y*20,20,20);
            }
            else if (element == " F ") {
                ctx.fillStyle = "black"
                ctx.fillRect(x*20,y*20,20,20);
            }
            else if (element == " J ") {
                ctx.fillStyle = "blue"
                ctx.fillRect(x*20,y*20,20,20);
            }
            else {
                ctx.fillStyle = "pink"
                ctx.fillRect(x*20,y*20,20,20);
            }
        });
      
    }); 
}
//mapa visualizado en la consola 
// function pintarMapa(){
//     var tablero = "";
//     //Recorro el array y asigno el caracter correspondiente a cada posicion
//     mapa.forEach(posicion => {
//         posicion.forEach(element => {
//             if (element == 1) {
//                 tablero = tablero + " X ";//pared
//             }
//             else if (element == " F ") {
//                 tablero = tablero + " ■ ";//fantasma
//             }
//             else if (element == " J ") {
//                 tablero = tablero + " J ";//jugador
//             }
//             else {
//                 tablero = tablero + "   ";//pasillo
//             }
//         });
//         tablero += "\n";
//     }); 
//      console.log(tablero);
// }

//comprueba si en la siguiente casilla dependiendo de la direccion hay una pared, si no hay una pared en esa direccion devuelve true
function comprobarPared(direccion, direcy, direcx){
    switch (direccion){
        case "abajo": if (mapa[direcy + 1][direcx] != 1 ) {
            return true;
        }break;

        case "arriba": if (mapa[direcy - 1][direcx] != 1) {
            return true;
        }break;

        case "derecha": if(mapa[direcy][direcx + 1] != 1){
            return true;
        }break;

        case "izquierda": if(mapa[direcy][direcx - 1] != 1){
            return true;
        }break;
    }
    return false;
}

//comprueba en que direcciones no encuentra pared y las retorna en un array
function comprobarCruilla(direcy, direcx){
    var auxDirec = [];
    var aux;
    if (mapa[direcy + 1][direcx] != 1 ) {
        aux = "abajo";
        auxDirec.push(aux);
    }
    if (mapa[direcy - 1][direcx] != 1) {
        aux = "arriba";
        auxDirec.push(aux);
    }
    if (mapa[direcy][direcx + 1] != 1) {
        aux = "derecha";
        auxDirec.push(aux);
    }
    if (mapa[direcy][direcx - 1] != 1) {
        aux = "izquierda";
        auxDirec.push(aux);
    }
    return auxDirec;
}

//comprueba si el fantasma o el jugador se encuentra en una cruilla, en caso de que no tenga direccion inicial, haya una pared en la siguiente casilla o este en una cruilla
//(mas de una direccion) devolverá una direccion aleatoria de las disponibles en la cruilla, sino seguirá por misma
function direccion(direcy, direcx, direcActual) {
    var auxDirec = comprobarCruilla(direcy, direcx);
    if(direcActual =="" || comprobarPared(direcActual, direcy, direcx)==false || auxDirec.length > 2){
        var direccionFinal = Math.floor((Math.random() * auxDirec.length));
        return auxDirec[direccionFinal];
    }else{
        return direcActual;
    }
    
}

var intervalo = setInterval(function () {
    movimientoJugador(jugador);
    movimientoFantasmas(fantasma1);
    movimientoFantasmas(fantasma2);
    movimientoFantasmas(fantasma3);
    pintarMapaCanvas();
    //pintarMapa();
    puntuar();
    if(fin == true){
        morir();
    }
    //console.log("\n\n");
}, 1000);

//Hace que la posicion del fantasma se cambie en el mapa y comprueba si este se ha encontrado con un fantasma, dependiendo de la direccion que tiene sumará en x o y
function movimientoFantasmas(fantasma) {
    fantasma.direc = direccion(fantasma.y, fantasma.x, fantasma.direc);

    switch(fantasma.direc){
        case "derecha":
            if(mapa[fantasma.y][fantasma.x + 1] == " J "){
                fin = true;
            }
            mapa[fantasma.y][fantasma.x] = "  "
            mapa[fantasma.y][fantasma.x + 1] = " F "
            fantasma.x = fantasma.x + 1;
            break;
        case "izquierda":
            if(mapa[fantasma.y][fantasma.x - 1] == " J "){
                fin = true;
            }
            mapa[fantasma.y][fantasma.x] = "  "
            mapa[fantasma.y][fantasma.x - 1] = " F "
            fantasma.x = fantasma.x - 1;
            break;
        case "arriba":
            if(mapa[fantasma.y - 1][fantasma.x] == " J "){
                fin = true;
            }
            mapa[fantasma.y][fantasma.x] = "  "
            mapa[fantasma.y-1][fantasma.x] = " F "
            fantasma.y = fantasma.y - 1;
        break;
        case "abajo":
            if(mapa[fantasma.y +1][fantasma.x] == " J "){
                fin = true;
            }
            mapa[fantasma.y][fantasma.x] = "  "
            mapa[fantasma.y + 1][fantasma.x] = " F "
            fantasma.y = fantasma.y + 1;
        break;
    }
}

//Hace que la posicion del jugador se cambie en el mapa y comprueba si este se ha encontrado con un fantasma, dependiendo de la direccion que tiene sumará en x o y
//en caso de que se haya pulsado una de las flechas no moverá de manera aleatoria sino en la direccion pulsada
function movimientoJugador(jugador) {
    
    if(presion == false){
        jugador.direc = direccion(jugador.y, jugador.x, jugador.direc);
    }
    
    switch(jugador.direc){
        case "derecha":
            if(mapa[jugador.y][jugador.x + 1] == " F "){
                fin = true;
            }
            mapa[jugador.y][jugador.x] = "  "
            mapa[jugador.y][jugador.x + 1] = " J "
            jugador.x = jugador.x + 1;
            break;
        case "izquierda":
            if(mapa[jugador.y][jugador.x - 1] == " F "){
                fin = true;
            }
            mapa[jugador.y][jugador.x] = "  "
            mapa[jugador.y][jugador.x - 1] = " J "
            jugador.x = jugador.x - 1;
            break;
        case "arriba":
            if(mapa[jugador.y - 1][jugador.x] == " F "){
                fin = true;
            }
            mapa[jugador.y][jugador.x] = "  "
            mapa[jugador.y - 1][jugador.x] = " J "
            jugador.y = jugador.y - 1;
        break;
        case "abajo":
            if(mapa[jugador.y + 1][jugador.x] == " F "){
                fin = true;
            }
            mapa[jugador.y][jugador.x] = "  "
            mapa[jugador.y + 1][jugador.x] = " J "
            jugador.y = jugador.y + 1;
        break;
    }
}

//funcion para cambiar la direccion a la que cambiara el jugador con las flechas, se llama desde el html 
function detectKey (event){
    presion = true;
   // var x = event.which || event.keyCode;
    var direccion = "";
    switch(event.keyCode){
        case 39: if(mapa[jugador.y][jugador.x + 1] != 1){
            direccion = "derecha";
            }
            break;
        case 37: if(mapa[jugador.y][jugador.x - 1] != 1){
            direccion = "izquierda";
            } 
            break;
        case 38: if(mapa[jugador.y - 1][jugador.x] != 1){
            direccion = "arriba";
            }
            break;
        case 40: if(mapa[jugador.y + 1][jugador.x] != 1){
            direccion = "abajo";
            }  
            break;
    }
    jugador.direc = direccion;
}

//Limpia en intervala y muestra un mensaje de game over en el canvas
function morir(){
    clearInterval(intervalo);  
    ctx.font = "30px Arial";
    ctx.fillStyle = "red";
    ctx.fillText("GAME OVER PAQUETE", 150, 300);
    setPuntuacionMax();
    puntuacion = 0;
    //console.log("GAME OVER PAQUETE");
}

//se coje la puntuacion guardada en la cookie y se parsea a entero
function getPuntuacionMax(){
    var aux = document.cookie.split("=");
    return parseInt(aux[1]);
 }

//en caso de que la puntuacion actual sea mas grande que la guardad en la cookie se substituye
function setPuntuacionMax(){
    var aux = document.cookie.split("=");
    var aux2 = parseInt(aux[1]);

    if(getPuntuacionMax() < puntuacion || aux2 == 0 || aux[0] == "" ){
        document.cookie = "PuntuacionMax=" + puntuacion;
    }
}

//suma la puntuacion actual
function puntuar(){
    puntuacion = puntuacion + 1;
    ctx.font = "20px Arial";
    ctx.fillStyle = "black";
    ctx.fillText("Puntuacion " + puntuacion, 10, 620);
    ctx.fillText("PuntuacionMax " + getPuntuacionMax(), 150, 620);
}

start();
